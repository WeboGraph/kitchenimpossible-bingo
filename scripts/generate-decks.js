const path = require('path');
const fs = require('fs');

function parseCSV (csv) {
  const lines = csv.split(/\r\n/);

  return lines;
}

(async () => {
  const dataDirPath = path.join(__dirname, '../data/bullshitbingo');
  const outputDirPath = path.join(__dirname, '../tmp');

  const result = await fs.readdirSync(dataDirPath, { withFileTypes: true });
  const files = result.reduce((acc, cur) => {
    return cur.isFile()
      ? [ ...acc, { name: cur.name, path: path.join(dataDirPath, cur.name) } ]
      : acc
  }, []);

  const promises = files.map((file) => {
    const content = fs.readFileSync(file.path, { encoding: 'UTF-8' });

    return { ...file, content }
  });

  Promise.all(promises)
    .then(files => {
      return files.map(file => ({ ...file, content: parseCSV(file.content) }));
    })
    .then(files => {
      for (let file of files) {
        const outputPath = path.join(__dirname, '../static/decks', file.name.replace('.csv', '.json'));
        fs.writeFileSync(outputPath, JSON.stringify(file.content));
      }
    });
})();
