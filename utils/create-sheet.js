export default (words, width = 5, height) => {
  if (!height) height = width;

  const selected = [];
  const wordsCount = words.length;
  const amount = width * height;

  while (selected.length < amount) {
    const randPos = Math.floor(Math.random() * wordsCount);

    if (selected.includes(randPos)) {
      continue;
    } else {
      selected.push(randPos);
    }
  }

  const sheetRaw = selected.map((pos, index) => {
    return { active: false, bingo: false, text: words[pos]}
  });
  const sheet = [];

  for (let row = 0; row < height; row++) {
    sheet.push(sheetRaw.splice(0, width));
  }

  return sheet;
};
